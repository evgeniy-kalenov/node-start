var db = require('db');
var log = require('logger')(module);    // паттерн фабрика

function User(name){
    this.name = name;
}

User.prototype.hello = function(who){
    log(db.getPhrase("Hello") + ", " + who.name);
}

//exports.user = User;
module.exports = User;

/*
Объект module. Свойства:
id - содержит полный путь к файлу (используется node)
exports - экспортированные данный в модуле
parent - ссылка на родительский модуль, то есть тот который делает require данного модуля
filename - полное имя файла
loaded - показывает загрузился ли модуль
children - модули которые дынный модуль подключил через require
path - текущий пут и все выше него в которых node будет искать модуль .. (используется node)
*/
//console.log(module);